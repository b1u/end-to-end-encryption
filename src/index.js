import https from 'https';
import http from 'http';

const defaultOptions = {
    "path": "/blue/"
}

const serverResponse = {
    "server": "Blue",
    "language": "Node.js"
}

const notFoundResponse = {
    "error": true,
    "errorText": "Path not found."
}

const invalidURLResponse = {
    "error": true,
    "errorText": "URL is invalid."
}
const noURLResponse = {
    "error": true,
    "errorText": "No URL provided."
}

const invalidHeadersResponse = {
    "error": true,
    "errorText": "Headers are invalid."
}

function validateOptions(options) {
    if (typeof options == 'object' &&
        !Array.isArray(options) &&
        options !== null
    ) {
        if (options.path) {
            if (typeof options.path !== "string") {
                options.path = defaultOptions.path
            }
        } else {
            options.path = defaultOptions.path
        }
    } else {
        return {
            "path": defaultOptions.path
        }
    }

    return options;
}

function blue(options = {}) {
    const validatedOptions = validateOptions(options);

    return function (req, res, next) {
        if (req.url.startsWith(validatedOptions.path)) {
            if (req.url == validatedOptions.path) {
                res.status(200).json(serverResponse)
            } else if (req.url == validatedOptions.path + "request") {
                let url;
                let reqHeaders = {};

                if (req.headers["x-blue-url"]) {
                    try {
                        url = new URL(req.headers["x-blue-url"])
                    } catch {
                        return res.status(400).json(invalidURLResponse)
                    }
                } else {
                    return res.status(400).json(noURLResponse)
                }

                if (req.headers["x-blue-headers"]) {
                    try {
                        reqHeaders = JSON.parse(req.headers["x-blue-headers"])
                    } catch {
                        return res.status(400).json(invalidHeadersResponse)
                    }
                }

                const options = {
                    hostname: url.host,
                    path: url.pathname,
                    method: req.method,
                    headers: reqHeaders
                };

                const serverReq = (url.protocol == "https:" ? https : http).request(options, function (serverRes) {
                    const resHeaders = Object.assign({}, serverRes.headers)
                    res.set("content-type", "text/plain")
                    res.set("x-blue-status", serverRes.statusCode)
                    res.set("x-blue-headers", JSON.stringify(resHeaders))
                    let body = [];

                    serverRes.on("data", function (chunk) {
                        body.push(chunk);
                    });

                    serverRes.on("end", function () {
                        res.end(Buffer.concat(body));
                    });
                });

                serverReq.end();
                return;
            } else {
                res.status(404).json(notFoundResponse)
                return;
            }
        }
        next()
    }
}

export { blue };