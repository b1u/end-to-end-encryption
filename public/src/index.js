const defaultOptions = {
    "path": "/blue/"
}

async function validateOptions(options) {
    if (typeof options == 'object' &&
        !Array.isArray(options) &&
        options !== null
    ) {
        if (options.path) {
            if (typeof options.path !== "string") {
                options.path = defaultOptions.path
            }
        } else {
            options.path = defaultOptions.path
        }
    } else {
        return {
            "path": defaultOptions.path
        }
    }

    return options;
}

async function testServer(path) {
    try {
        const server = await fetch(path)
        const data = await server.json();

        if (data.server == "Blue") {
            return true;
        }

        return false;
    } catch {
        return false;
    }
}

class blueClient {
    constructor(options = {}) {
        (async () => {
            const validatedOptions = await validateOptions(options);
            this.options = options

            const connected = await testServer(this.options.path)

            this.connected = connected
        })()
    }
    async fetch(url, options = {}) {
        if (!options.headers) {
            options.headers = {};
        }

        if (this.connected) {
            const blueRequest = await fetch(this.options.path + "request", {
                headers: {
                    "x-blue-url": String(url),
                    "x-blue-headers": JSON.stringify(options.headers)
                }
            })

            return {
                "response": blueRequest,
                "status": blueRequest.headers.get("x-blue-status"),
                "headers": JSON.parse(blueRequest.headers.get("x-blue-headers"))
            }
        }
    }
}

self.blueClient = blueClient;