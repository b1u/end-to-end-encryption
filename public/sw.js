importScripts("/src/index.js")

const blue = new blueClient({
    path: "/blue/"
})

self.addEventListener("fetch", async (e) => {
    const url = new URL(e.request.url);

    if (url.pathname == "/test") {
        const newURL = new URL(url).searchParams.get("url")
        const reqHeaders = Object.fromEntries([...e.request.headers])

        reqHeaders["host"] = newURL.host
        delete reqHeaders["accept-encoding"]

        e.respondWith((async () => {
            const req = await blue.fetch(newURL, {
                headers: reqHeaders
            })
            const body = await req.response.arrayBuffer()

            return new Response(body, {
                status: req.status,
                headers: req.headers
            })
        })())
    }
})