import express from 'express';
import path from 'path';
import { blue } from './src/index.js';
const app = express()
const port = process.env.PORT || 3000
const __dirname = path.resolve();

app.use(blue({ path: "/blue/" }))

app.get('/', (req, res) => {
  res.sendFile(__dirname + "/public/index.html")
})

app.use(express.static("public"));

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})
